# -*- coding: utf-8 -*-

from datetime import datetime, date
from pony.orm import *
import settings


db = Database()

db.bind(provider='sqlite', filename=settings.DB_FILE, create_db=True)


class Car(db.Entity):
    _table_ = "cars"
    sorgente_id = Required(unicode)
    sorgente = Required(unicode)
    titolo_annuncio = Required(unicode)
    uuid = Required(unicode)
    marca = Required(unicode)
    modello = Required(unicode)
    carburante = Optional(unicode)
    cambio = Optional(unicode)
    anno_imm = Required(int)
    mese_imm = Optional(int)
    km_num = Required(int)
    km_range = Required(unicode)
    allestimento = Optional(unicode)
    dove = Optional(unicode)
    colore = Optional(unicode)
    regione = Optional(unicode)
    provincia = Optional(unicode)
    cap = Optional(unicode)
    sorgente_user_id = Required(unicode)
    sorgente_username = Optional(unicode)
    ultimo_scrape = Required(datetime)
    descrizione_utente = Optional(unicode)
    sorgente_url = Required(unicode)
    tipo_inserzionista = Optional(unicode)
    prezzo = Required(int)
    pubblicato_il = Optional(datetime)
    anzianita_giorni = Optional(int)
    annuncio_padre_uuid = Optional(unicode)
    trazione = Optional(unicode)
    nome_versione = Optional(unicode)
    cilindrata = Optional(unicode)
    created = Optional(datetime)


class PriceChange(db.Entity):
    _table_ = "price_change"
    uuid = Required(unicode)
    vecchio_prezzo = Required(int)
    nuovo_prezzo = Required(int)
    created = Required(datetime)


class Spec(db.Entity):
    _table_ = "specs"
    label = Required(unicode)
    aliases = Optional(unicode)
    category = Optional(unicode)
    created = Optional(datetime)
    modified = Optional(datetime)
    priority = Optional(int)
    hide = Optional(int)
    models_t = Set('ModelSpec')


class Model(db.Entity):
    _table_ = "models"
    marca = Required(unicode)
    name = Required(unicode)
    url = Required(unicode)
    automoto_id = Required(int)
    years = Optional(unicode)
    start_date = Optional(date)
    end_date = Optional(date)
    created = Optional(datetime)
    modified = Optional(datetime)
    price = Required(unicode)
    specs_t = Set('ModelSpec')


class ModelSpec(db.Entity):
    _table_ = "model_specs"
    model = Required(Model, column='model_id')
    spec = Required(Spec, column='spec_id')
    tipo = Required(unicode)
    value = Required(unicode)
    created = Optional(datetime)
    modified = Optional(datetime)


db.generate_mapping(create_tables=True)
