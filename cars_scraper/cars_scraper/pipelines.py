# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import logging
from datetime import datetime
from scrapy.exceptions import DropItem
from pony.orm import *
from orm import Car, PriceChange

logger = logging.getLogger(__name__)


class CarsScraperPipeline(object):

    def open_spider(self, spider):
        self.passati = 0
        self.nuovi = 0
        self.reinseriti = 0
        self.aggiornato_prezzo = 0
        self.duplicati_su_sorgenti_diverse = 0

    def close_spider(self, spider):
        logger.info("PIPELINE::: Passati: {}. Nuovi: {}. Reinseriti: {}. Aggiornato prezzo: {}. Duplicati su sorgenti diverse: {}".format(
            self.passati,
            self.nuovi,
            self.reinseriti,
            self.aggiornato_prezzo,
            self.duplicati_su_sorgenti_diverse
        ))
        pass

    @db_session
    def process_item(self, item, spider):

        self.passati += 1

        # controllo se c'è già id + sorgente
        car = select(c for c in Car if c.sorgente == item['sorgente'] and c.sorgente_id == item['sorgente_id']).first()

        if car:
            logger.debug("Auto già presente in database. Aggiorno..")
            if car.prezzo != item['prezzo']:
                # registro la variazione
                pc = PriceChange(
                    uuid=car.uuid,
                    vecchio_prezzo=car.prezzo,
                    nuovo_prezzo=item['prezzo'],
                    created=datetime.now()
                    )
                car.prezzo = item['prezzo']  # aggiorno solo questo per il momento. sarebbe interessante anche sapere le variazioni
                self.aggiornato_prezzo += 1
                logger.debug("Prezzo cambiato. Aggiorno..")

            #@TODO: update di alcuni dati
            car.km_range = item['km_range']
            car.km_num = item['km_num']
            car.ultimo_scrape = item['ultimo_scrape']

        else:
            # prossimo test: verifica utente
            car = select(c for c in Car if c.sorgente == item['sorgente'] \
                 and c.sorgente_user_id == item['sorgente_user_id'] \
                 and c.marca == item['marca'] \
                 and c.modello == item['modello'] \
                 and c.anno_imm == item['anno_imm']).first()

            if car:
                logger.warning("Nuovo annuncio ma semplicemente reinserito: aggiorno il db...")

                new_car = Car(**item)
                new_car.created = datetime.now()
                new_car.annuncio_padre_uuid = car.uuid
                self.reinseriti += 1

            else:
                # terzo caso: sorgente diversa, ma prezzo, modello, marca, provincia, carburante, anno_imm uguali

                #

                cars = select(c for c in Car
                     if c.sorgente != item['sorgente'] \
                     and c.marca == item['marca'] \
                     and c.modello == item['modello'] \
                     and c.prezzo == item['prezzo'] \
                     and c.carburante == item['carburante'] \
                     and c.anno_imm == item['anno_imm'])

                if cars.count():

                    for possible_dup in cars:
                    # queste auto sono potenzialmente uguali
                        if len(item['cap']) and item['cap'] == possible_dup.cap:
                            # confermo doppione
                            logger.warning("Duplicato su sorgenti diverse...")
                            self.duplicati_su_sorgenti_diverse += 1
                            car = possible_dup

                        elif possible_dup.km_num > 0 \
                            and item['km_num'] \
                            and item['km_num']-10000 < possible_dup.km_num \
                            and possible_dup.km_num < item['km_num']+10000:
                            # stessi km
                            logger.warning("Duplicato su sorgenti diverse...")
                            self.duplicati_su_sorgenti_diverse += 1
                            car = possible_dup


                # TODO
                else:
                    logger.debug("Inserimento nuovo record...")
                    # a questo punto è nuovo. Inserisco!
                    car = Car(**item)
                    car.created = datetime.now()
                    self.nuovi += 1

        # anzianità giorni
        if car:
            if car.pubblicato_il:
                car.anzianita_giorni = (datetime.now() - car.pubblicato_il).days
            else:
                car.anzianita_giorni = (datetime.now() - car.created).days if car.created else None
