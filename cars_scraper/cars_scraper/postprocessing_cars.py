# -*- coding: utf-8 -*-

import logging
from pony.orm import *
from orm import *

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")

logger = logging.getLogger(__name__)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)
logger.setLevel(logging.DEBUG)

logger.info("Starting data postprocessing...")


def cerca_match_in_campi(car, field, str_to_search, val_to_set=None):
    if not val_to_set:
        val_to_set = str_to_search

    if any(str_to_search in x for x in [car.titolo_annuncio.lower(), car.allestimento.lower(), car.descrizione_utente.lower()]):
        setattr(car, field, val_to_set)
        return True
    return False


def cerca_match_in_campi_list(car, field, str_to_search_list, val_to_set):
    # for s in str_to_search_list:
    return any(cerca_match_in_campi(car, field, s, val_to_set) for s in str_to_search_list)


def correggi_modello(marca, parte_di_modello, modello_da_assegnare):

    fixed = 0
    cars_no_feature = select(c for c in Car if c.marca == marca and parte_di_modello in c.modello)
    for car in cars_no_feature:
        car.modello = modello_da_assegnare
        fixed += 1

    logger.info("Corretto modello '{}' per {} annunci".format(modello_da_assegnare, fixed))




with db_session:
    fixed = 0
    cars_no_feature = select(c for c in Car if c.carburante == '')

    logger.info("Trovati {} annunci senza carburante".format(cars_no_feature.count()))
    for car in cars_no_feature:
        # indoviniamo il carburante?
        if any("crdi" in x for x in [car.titolo_annuncio.lower(), car.allestimento.lower(), car.descrizione_utente.lower()]):
            car.carburante = "diesel"
            fixed += 1
        elif any("diesel" in x for x in [car.titolo_annuncio.lower(), car.allestimento.lower(), car.descrizione_utente.lower()]):
            car.carburante = "diesel"
            fixed += 1
        elif any("gdi" in x for x in [car.titolo_annuncio.lower(), car.allestimento.lower(), car.descrizione_utente.lower()]):
            car.carburante = "benzina"
            fixed += 1

    logger.info("Sistemati {} annunci per il carburante".format(fixed))

    fixed = 0
    cars_no_feature = select(c for c in Car if c.cambio == '')

    logger.info("Trovati {} annunci senza cambio".format(cars_no_feature.count()))
    for car in cars_no_feature:
        # indoviniamo il carburante?
        if any("awd" in x for x in [car.titolo_annuncio.lower(), car.allestimento.lower(), car.descrizione_utente.lower()]):
            car.cambio = "automatico"
            fixed += 1

    logger.info("Sistemati {} annunci per il cambio".format(fixed))


    # settiamo la versione per X
    modello = "ix35"
    fixed = 0
    cars_no_feature = select(c for c in Car if (c.nome_versione == '' or c.nome_versione is None) and c.modello == modello)


    logger.info("Trovati {} annunci senza nome_versione per {}".format(cars_no_feature.count(), modello))
    for car in cars_no_feature:
        if cerca_match_in_campi_list(car, 'nome_versione', ['classic', ], 'classic'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['xpossible', 'x possible', 'x-possible'], 'xpossible'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['style', ], 'style'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['comfort', 'confort'], 'comfort'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', 'high', 'high'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', 'full optional', 'high'):
            fixed + 1

    logger.info("Sistemati {} annunci senza nome_versione per {}".format(fixed, modello))


    # cilindrata per Hyundai
    fixed = 0
    cars_no_feature = select(c for c in Car if (c.cilindrata == '' or c.cilindrata is None) and c.modello == "ix35")


    logger.info("Trovati {} annunci senza cilindrata".format(cars_no_feature.count()))
    for car in cars_no_feature:
        if cerca_match_in_campi(car, 'cilindrata', '1.7', '1.7'):
            fixed + 1
        elif cerca_match_in_campi(car, 'cilindrata', '1700', '1.7'):
            fixed + 1
        elif cerca_match_in_campi(car, 'cilindrata', '2.0', '2.0'):
            fixed + 1
        elif cerca_match_in_campi(car, 'cilindrata', '2000', '2.0'):
            fixed + 1

    logger.info("Settata cilindrata per {} annunci".format(fixed))


### KIA
    correggi_modello('kia', 'sportage', 'sportage')

    # settiamo la versione per X
    modello = "sportage"

    fixed = 0
    cars_no_feature = select(c for c in Car if (c.nome_versione == '' or c.nome_versione is None) and c.modello == modello)

    logger.info("Trovati {} annunci senza nome_versione per {}".format(cars_no_feature.count(), modello))

    for car in cars_no_feature:
        if cerca_match_in_campi_list(car, 'nome_versione', ['class', ], 'class'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['active', ], 'active'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['plus', 'cool', ], 'cool'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['rebel', ], 'feel rebel'):
            fixed + 1

    logger.info("Sistemati {} annunci senza nome_versione per {}".format(fixed, modello))


    # cilindrata

    fixed = 0
    cars_no_feature = select(c for c in Car if (c.cilindrata == '' or c.cilindrata is None)  and c.modello == modello)

    logger.info("Trovati {} annunci senza cilindrata per {}".format(cars_no_feature.count(), modello))

    for car in cars_no_feature:
        if cerca_match_in_campi_list(car, 'cilindrata', ['1.6', '1600' ], '1.6'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'cilindrata', ['1.7', '1700' ], '1.7'):
            fixed + 1
        elif cerca_match_in_campi_list(car, 'cilindrata', ['2.0', '2000', '1995' ], '2.0'):
            fixed + 1

    logger.info("Sistemati {} annunci senza cilindrata per {}".format(fixed, modello))


### QASHQAI
    correggi_modello('nissan', 'qashqai', 'qashqai')


    # settiamo la versione per X
    modello = "qashqai"
    fixed = 0
    cars_no_feature = select(c for c in Car if (c.nome_versione == '' or c.nome_versione is None) and c.modello == modello)

    logger.info("Trovati {} annunci senza nome_versione per {}".format(cars_no_feature.count(), modello))

    for car in cars_no_feature:
        if cerca_match_in_campi_list(car, 'nome_versione', ['business', 'busines' ], 'business'):
            fixed += 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['tekna', 'tecna' ], 'tekna'):
            fixed += 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['visia', ], 'visia'):
            fixed += 1
        elif cerca_match_in_campi_list(car, 'nome_versione', ['connecta', 'connetta', 'conneta' ], 'connecta'):
            fixed += 1

    logger.info("Sistemati {} annunci senza nome_versione per {}".format(fixed, modello))
    # la versione base è la Visia

### KUGA
    correggi_modello('ford', 'kuga', 'kuga')

    # per ora non gestito



    # 4wd mancate
    fixed = 0
    cars_no_feature = select(c for c in Car)

    logger.info("Rivedo tutti e {} gli annunci per la trazione".format(cars_no_feature.count()))
    for car in cars_no_feature:
        if cerca_match_in_campi(car, 'trazione', '4wd', '4WD'):
            fixed + 1
        if cerca_match_in_campi(car, 'trazione', '4x4', '4WD'):
            fixed + 1
        if cerca_match_in_campi(car, 'trazione', '2x4', '2WD'):
            fixed + 1
        if cerca_match_in_campi(car, 'trazione', '2wd', '2WD'):
            fixed + 1

    logger.info("Risettata trazione per {} annunci".format(fixed))