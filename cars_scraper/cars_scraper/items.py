# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class UsedCarItem(scrapy.Item):
    sorgente_id = scrapy.Field()
    sorgente = scrapy.Field()
    uuid = scrapy.Field()
    titolo_annuncio = scrapy.Field()
    marca = scrapy.Field()
    modello = scrapy.Field()
    carburante = scrapy.Field()
    cambio = scrapy.Field()
    anno_imm = scrapy.Field()
    mese_imm = scrapy.Field()
    km_num = scrapy.Field()
    km_range = scrapy.Field()
    allestimento = scrapy.Field()
    dove = scrapy.Field()
    colore = scrapy.Field()
    regione = scrapy.Field()
    provincia = scrapy.Field()
    cap = scrapy.Field()
    sorgente_user_id = scrapy.Field()
    sorgente_username = scrapy.Field()
    sorgente_url = scrapy.Field()
    ultimo_scrape = scrapy.Field()
    descrizione_utente = scrapy.Field()
    sorgente_url = scrapy.Field()
    tipo_inserzionista = scrapy.Field()
    prezzo = scrapy.Field()
    pubblicato_il = scrapy.Field()
    trazione = scrapy.Field()


