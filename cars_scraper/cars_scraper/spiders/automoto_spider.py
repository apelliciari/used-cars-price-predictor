# -*- coding: utf-8 -*-

import scrapy
import urlparse
import urllib
import re
import json
import locale
from datetime import datetime
import sys
from dateutil import parser

from os.path import join
from cars_scraper import settings

from pony.orm import *
from cars_scraper.orm import *

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


locale.setlocale(locale.LC_ALL, 'ita')  # diverso su ubuntu, dovrebbe essere it_it


class AutomotoSpider(scrapy.Spider):
    name = 'automoto'
    start_urls = [
        'http://www.automoto.it/catalogo/kia/sportage/sportage--2010--',
        'http://www.automoto.it/catalogo/hyundai/ix35/ix35--2010--',
    ]
    custom_settings = {
        'ITEM_PIPELINES': {

        }
    }
    page_number = 1

    @db_session
    def parse(self, response):
        # per sapere che sta facendo un nuovo parsing...
        sys.stdout.write('.')

        url_tokens = response.url.split("/")
        if is_number(url_tokens[-1]):
            model_id = int(url_tokens[-1])
            # pagina di dettaglio
            self.logger.debug("annuncio:: {}".format(response.url))

            # vedo se ho già il modello in canna
            # check su model_id

            model = select(m for m in Model if m.automoto_id == model_id).first()

            if not model:
                model = Model(name=response.css("h1::text").extract_first().strip(),
                    marca=response.css("#cpContent_ctl00_sBrand::text").extract_first().strip(),
                    years=response.css("span.years::text").extract_first().strip(),
                    price=response.css("div.price span.value::text").extract_first().strip(),
                    url=response.url, automoto_id=model_id, created=datetime.now(), modified=datetime.now())

            regex = r"(\d{2}\/\d{4})"

            matches = re.finditer(regex, model.years)

            if len(matches) == 2:
                model.start_date = matches.group(0)
                model.end_date = matches.group(1)


            html_specs = response.css("div.datasheet>*, div.vehicle-addons>*")
            category = ""

            for html_spec in html_specs:
                if "<h2" in html_spec.extract():
                    category = html_spec.css("::text").extract_first().strip()

                    if not category:
                        category = html_spec.css("span::text").extract_first().strip()

                else:
                    table_els = html_spec.css("table.datagrid tr")

                    for el in table_els:
                        label = el.css("th::text").extract_first().strip()
                        value = el.css("td::text").extract_first().strip()

                        if value == "di serie":
                            value = "x"

                        if el.css("ul"):
                            value = el.xpath(".//ul//text()").extract_first()

                        spec = select(s for s in Spec if s.label == label).first()

                        if not spec:

                            spec = Spec(label=label, category=category if category.lower() not in ["accessori di serie", "optionals"] else "",
                                        created=datetime.now(), modified=datetime.now())

                        model_spec = select(ms for ms in ModelSpec if ms.model == model and ms.spec == spec).first()

                        if not model_spec:
                            if category.lower() == "accessori di serie":
                                tipo = "di serie"
                            elif category.lower() == "optionals":
                                tipo = "optional"
                            else:
                                tipo = "caratteristica"

                            model_spec = ModelSpec(
                                    model=model, spec=spec, value=value, tipo=tipo, created=datetime.now(), modified=datetime.now())

            commit()

        else:  # listing /risultati?

            self.logger.debug("listing:: {}".format(response.url))
            for table in response.css(".vehicle-list table"):
                if table.css("th.group div::text").extract_first() == "Diesel":
                    v_urls = table.css("td.name a::attr(href)").extract()

                    for url in v_urls:
                        yield response.follow("{}{}".format("http://www.automoto.it", url), self.parse)
