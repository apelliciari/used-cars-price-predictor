# -*- coding: utf-8 -*-

import scrapy
import urlparse
import urllib
import re
import json
import locale
import datetime
import sys
from dateutil import parser

from os.path import join
from cars_scraper import settings
from cars_scraper.items import UsedCarItem

from pony.orm import *
from ..orm import Car

from objectpath import *

locale.setlocale(locale.LC_ALL, 'ita')  # diverso su ubuntu, dovrebbe essere it_it

# carico file json
with open(settings.DB_COMUNI_ITALIANI) as thefile:
    comuni = json.load(thefile)

def get_comune_by_cap(cap):
    for comune in comuni:
        if cap in comune['cap']:
            return comune

class AutoscoutSpider(scrapy.Spider):
    name = 'autoscout'
    sorgente = "autoscout.it"
    start_urls = [
        'https://www.autoscout24.it/risultati?mmvmk0=33&mmvmd0=19258&mmvco=1&fregfrom=2009&pricefrom=0&cy=I&powertype=kw&atype=C&ustate=N%2CU&sort=standard&desc=0&PageSpeed=noscript&page=1&size=20', # ix35 dal 2009
        'https://www.autoscout24.it/risultati?mmvmk0=39&mmvmd0=1812&mmvco=1&fregfrom=2009&pricefrom=0&cy=I&powertype=kw&atype=C&ustate=N%2CU&sort=standard&desc=0&page=1&size=20',   # kia sportage > 2009
        'https://www.autoscout24.it/risultati?mmvmk0=52&mmvmd0=18921&mmvco=1&fregfrom=2009&pricefrom=0&cy=I&powertype=kw&atype=C&ustate=N%2CU&sort=standard&desc=0&page=1&size=20',  # qashqai > 2009
        'https://www.autoscout24.it/risultati?mmvmk0=29&mmvmd0=19072&mmvco=1&fregfrom=2009&pricefrom=0&cy=I&powertype=kw&atype=C&ustate=N%2CU&sort=standard&desc=0&page=1&size=20',  # kuga > 2009
    ]
    page_number = 1
    ads_passed = 0


    def closed(self, reason):
        self.logger.info("Passati {} annunci.".format(self.ads_passed))

    @db_session
    def parse(self, response):
        # per sapere che sta facendo un nuovo parsing...
        sys.stdout.write('.')

        if "/annunci/" in response.url:
            # pagina di dettaglio
            self.logger.debug("annuncio:: {}".format(response.url))
            car = UsedCarItem()

            try:
                tagline_cambio_carburante = response.css("div.cldt-stage-basic-data div:nth-child(3) span.cldt-stage-att-description::text").extract_first().lower()
                cambio, carburante = tagline_cambio_carburante.split(",")
            except ValueError as e:
                # self.logger.warning("Errore nell'unpack di cambio e carburante. Valore trovato: '{}'".format(tagline_cambio_carburante))
                if tagline_cambio_carburante.strip() in settings.CARBURANTI:
                    carburante = tagline_cambio_carburante
                else:
                    carburante = ""
                cambio = ""

            car['sorgente_url'] = response.url

            car['sorgente'] = self.sorgente
            car['titolo_annuncio'] = response.css("div.cldt-headline h1::text").extract_first()
            car['sorgente_id'] = self.gtm_meta('classified_productID', response.body)
            car['uuid'] = "{}-{}".format(car['sorgente'], car['sorgente_id'])
            car['marca'] = self.gtm_meta('classified_makeTxt', response.body)
            car['modello'] = self.gtm_meta('classified_modelTxt', response.body)
            car['carburante'] = carburante.strip()
            car['cambio'] = self.dd_value('Tipo di cambio', response.body) if self.dd_value('Tipo di cambio', response.body) else cambio # le provo tutte
            car['prezzo'] = locale.atoi(self.gtm_meta('classified_price', response.body))
            car['anno_imm'] = self.gtm_meta('classified_year', response.body)
            car['mese_imm'] = self.gtm_meta('classified_month', response.body)
            car['km_range'] = self.gtm_meta('classified_mileage', response.body)
            car['km_num'] = locale.atoi(car['km_range'])
            car['dove'] = ""
            car['colore'] = self.dd_value('Colore esterno', response.body) if self.dd_value('Colore esterno', response.body) else ""

            car['regione'] = "" ## lookup CAP
            car['provincia'] = "" ## lookup CAP
            car['cap'] = self.gtm_meta('classified_zipcode', response.body)  # non c'è su subito
            car['tipo_inserzionista'] = self.gtm_meta('classified_customer_type', response.body)

            if car['tipo_inserzionista'] == "P":
                car['tipo_inserzionista'] = "privato"

            if car['cap']:
                comune = get_comune_by_cap(car['cap'])
                if comune:
                    car['regione'] = comune['regione']['nome'].lower()
                    car['provincia'] = comune['provincia']['nome'].lower() if comune['provincia']['nome'] else comune['cm']['nome'].lower()

            # if pubblicato_str:
            car['pubblicato_il'] = None  # autoscout non ha la data di pubblicazione

            # trazione
            if any(x in car['titolo_annuncio'] for x in settings.TRAZIONI_4WD):
                car['trazione'] = "4WD"
            else:
                # supponiamo che sia a 2
                car['trazione'] = "2WD"

            car['descrizione_utente'] = response.css("div[data-item-name='description'] div[data-type='description']::text").extract()
            car['descrizione_utente'] = r"\r\n".join(car['descrizione_utente'])

            car['sorgente_user_id'] = self.gtm_meta("classified_customerID", response.body)
            car['ultimo_scrape'] = datetime.datetime.now()  # TODO
            # car['anzianita_giorni'] = None  # non disponibile (datetime.datetime.now() - car['pubblicato_il']).days if car['pubblicato_il'] else None # TODO

            car['anno_imm'] = datetime.datetime.today().year

            yield car

        else:  # listing /risultati?

            self.logger.debug("listing:: {}".format(response.url))

            for ad in response.css('div.cl-list-element div.cldt-summary-full-item'):
                prezzo_str = ad.css('span.cldt-price::text').extract_first().split(",")[0]
                prezzo = locale.atoi(prezzo_str.split()[1])
                ad_id_json = ad.css("as24-tracking::attr(as24-tracking-value)").extract_first()
                ad_id_dict = json.loads(ad_id_json)
                ad_id = ad_id_dict['id']

                # TODO: log totale annunci!
                self.ads_passed += 1

                car = select(c for c in Car if c.sorgente == self.sorgente  \
                    and c.sorgente_id == ad_id  \
                    and c.prezzo == prezzo).first()

                # riscarico solo se non c'è l'annuncio in db o è cambiato il prezzo
                if not car:
                    yield response.follow(ad.css("a[data-item-name=detail-page-link]::attr(href)").extract_first(), self.parse)
                else:
                    self.logger.debug("Skip annuncio '{}' perchè già in db".format(ad_id))


            if "Page number or size exceeded" not in response.body:
                # analizzo un'altra pagina

                self.page_number = self.page_number + 1

                up = urlparse.urlsplit(response.url)
                queryparams_dict = dict(urlparse.parse_qsl(up.query))

                queryparams_dict['page'] = self.page_number

                next_url = "{}://{}{}?{}".format(up.scheme, up.netloc, up.path, urllib.urlencode(queryparams_dict))

                yield response.follow(next_url, self.parse)

            else:
                self.logger.info("Raggiunta l'ultima pagina.")


    def gtm_meta(self, key, haystack, lowerize=True):
        # m = re.search(r'\{\"{}\":\s*\"([^\"]*)\"\}'.format(key), haystack)
        m = re.search(r'\{\"' + key + r'\":\s*\"([^\"]*)\"\}', haystack)

        if m:
            s = m.group(1)

            return s.lower() if lowerize else s  # solo il primo gruppo

        return None

    #TODO: aggiungere caso class="sc-ellipsis"
    def dd_value(self, key, haystack, lowerize=True):
        m = re.search(r'<dt>\s*\n*\r*{}\s*\n*\r*</dt>\s*\n*\r*<dd>\s*\n*\r*([^<\n]+)\s*\n*\r*</dd>'.format(key), haystack)
        if m:
            s = m.group(1)

            return s.lower() if lowerize else s  # solo il primo gruppo

        return None
