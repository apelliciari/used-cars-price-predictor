# -*- coding: utf-8 -*-

import scrapy
import urlparse
import urllib
import re
import locale
import logging
import datetime
from dateutil import parser
import sys

from os.path import join
from cars_scraper import settings
from cars_scraper.items import UsedCarItem

from pony.orm import *
from ..orm import Car



locale.setlocale(locale.LC_ALL, 'ita')  # diverso su ubuntu, dovrebbe essere it_it


class SubitoSpider(scrapy.Spider):
    name = 'subito'
    sorgente = "subito.it"
    start_urls = [
        'https://www.subito.it/annunci-italia/vendita/auto/?rs=2009&cb=000122&cm=001343',  # ix35 dal 2009
        'https://www.subito.it/annunci-italia/vendita/auto/?cb=000746&cm=000950&rs=2009',  # kia sportage dal 2009
        'https://www.subito.it/annunci-italia/vendita/auto/?cb=000149&cm=001149&rs=2009',  # nissan qashqai dal 2009
        'https://www.subito.it/annunci-italia/vendita/auto/?cb=000039&cm=001279&rs=2009',  # ford kuga dal 2009
    ]

    page_number = 1
    last_page = None
    ads_passed = 0

    def __init__(self, full_refresh=None, *args, **kwargs):
        super(SubitoSpider, self).__init__(*args, **kwargs)
        self.full_refresh = full_refresh


    def regex_tc_vars(self, key, val):
        m = re.search(r'{}\s*:\s*[\'\"]([^\'\"]*)[\'\"],'.format(key), val)
        if m:
            return m.group(1)  # solo il primo gruppo

        return None

    def closed(self, reason):
        self.logger.info("Passati {} annunci.".format(self.ads_passed))

    @db_session
    def parse(self, response):

        sys.stdout.write('.')

        if self.last_page is None:
            last_page_url = response.css('div.pagination div.pagination_bottom:nth-child(2) a::attr(href)').extract_first()
            last_page_url_qs = dict(urlparse.parse_qsl(urlparse.urlsplit(last_page_url).query))
            self.last_page = int(last_page_url_qs['o'])

        if "/annunci-italia/vendita/auto/" in response.url:
            # listing

            for ad in response.css("li article.item_list.view_listing"):
                prezzo_str = ad.css("div.item_list_section.item_description span.item_price::text").extract_first()
                prezzo = locale.atoi(prezzo_str.split()[0])
                ad_id = int(ad.css("::attr(data-id)").extract_first())

                # TODO: log totale annunci!
                self.ads_passed += 1

                car = select(c for c in Car if c.sorgente == self.sorgente  \
                    and c.sorgente_id == ad_id  \
                    and c.prezzo == prezzo).first()

                # riscarico solo se non c'è l'annuncio in db o è cambiato il prezzo
                if not car or self.full_refresh:
                    yield response.follow(ad.css("h2 a::attr(href)").extract_first(), self.parse)
                else:
                    self.logger.debug("Skip annuncio '{}' perchè già in db".format(ad_id))

            # for url in response.css('div.main div.listing ul.items_listing h2 a::attr(href)').extract():
            #     yield response.follow(url, self.parse)

            if self.page_number <= self.last_page:
                self.page_number = self.page_number + 1

                up = urlparse.urlsplit(response.url)
                queryparams_dict = dict(urlparse.parse_qsl(up.query))

                queryparams_dict['o'] = self.page_number

                next_url = "{}://{}{}?{}".format(up.scheme, up.netloc, up.path, urllib.urlencode(queryparams_dict))

                yield response.follow(next_url, self.parse)

        else:  # pagina dettaglio
            car = UsedCarItem()

            car['sorgente_url'] = response.url

            car['sorgente'] = self.sorgente
            car['titolo_annuncio'] = self.regex_tc_vars('ad_title', response.body)
            car['sorgente_id'] = self.regex_tc_vars('ad_id', response.body)
            car['uuid'] = "{}-{}".format(car['sorgente'], car['sorgente_id'])
            car['marca'] = self.regex_tc_vars('ad_veicoli_brand', response.body)
            car['modello'] = self.regex_tc_vars('ad_veicoli_model', response.body)
            car['carburante'] = self.regex_tc_vars('ad_veicoli_fuel', response.body)
            car['cambio'] = self.regex_tc_vars('ad_veicoli_gear', response.body)
            car['prezzo'] = locale.atoi(self.regex_tc_vars('ad_price', response.body))
            car['anno_imm'] = self.regex_tc_vars('ad_veicoli_year', response.body)
            car['mese_imm'] = None
            car['km_range'] = self.regex_tc_vars('ad_veicoli_km', response.body)
            car['dove'] = self.regex_tc_vars('ad_city', response.body)
            car['colore'] = self.regex_tc_vars('ad_veicoli_color', response.body)
            car['regione'] = self.regex_tc_vars('ad_region', response.body)
            car['provincia'] = self.regex_tc_vars('ad_province', response.body)
            car['cap'] = ""  # non c'è su subito
            car['tipo_inserzionista'] = self.regex_tc_vars('ad_advertiser_type', response.body)
            pubblicato_str = response.css("div.profile .data time::attr(datetime)").extract_first()   # 2017-09-24 11:20:11

            if pubblicato_str:
                car['pubblicato_il'] = parser.parse(pubblicato_str)

            car['sorgente_url'] = response.url

            # trazione
            if any(x in car['titolo_annuncio'] for x in settings.TRAZIONI_4WD):
                car['trazione'] = "4WD"
            else:
                # supponiamo che sia a 2
                car['trazione'] = "2WD"

            # NON va bene allestimento
            # tirare fuori tabella chiave valore e vedere se c'è allestimento
            trs = response.css("div#ad_details div.summary table tr")
            for tr in trs:
                tds = tr.css("td::text")

                if len(tds) == 2:
                    label, val = tds.extract()
                    if label.lower() == "allestimento":
                        car['allestimento'] = val.strip()

            car['descrizione_utente'] = response.css("div#ad_details div.description::text").extract_first()
            car['descrizione_utente'] = car['descrizione_utente'].strip() if car['descrizione_utente'] else ""

            car['sorgente_user_id'] = self.regex_tc_vars('ad_urn', response.body).split(":")[2]
            car['sorgente_username'] = response.css("div.profile strong.author.btn_author_reply::text").extract_first().strip()
            if car['km_range'].strip().lower() == "km 0":
                car['km_num'] = 0
            else:
                car['km_num'] = locale.atoi(car['km_range'].split("-")[1].strip()) + 2  # per il momento prendo il massimo del range, poi si potrebbe fare la media. aggiungo 1 cosi li riconosco
            car['ultimo_scrape'] = datetime.datetime.now()  # TODO
            # car['anzianita_giorni'] = (datetime.datetime.now() - car['pubblicato_il']).days if car['pubblicato_il'] else None # TODO

            yield car

