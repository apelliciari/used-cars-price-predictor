# -*- coding: utf-8 -*-

# due args: marca e nome file

from pony.orm import *
from orm import *
import sys

import pandas as pd


def get_val(model_spec):
    if model_spec.tipo == "caratteristica":
        return model_spec.value
    elif model_spec.tipo == "di serie":
        return "x"
    elif model_spec.tipo == "optional":
        return "O"

with db_session:
    models = select(m for m in Model if m.marca == sys.argv[1])

    final_df = pd.DataFrame()

    for model in models:
        df = pd.DataFrame(
            [('anno', model.years), ('prezzo', model.price), ('link scheda', model.url)]
            + [(x.spec.label, get_val(x)) for x in model.specs_t])
        df = df.set_index(0)  # setto le label come index
        df = df.rename(columns={1: model.name})

        final_df = pd.concat([final_df, df], axis=1)

        # rename prima colonna con nome model

final_df.to_excel("{}-allestimenti.xlsx".format(sys.argv[2], ))