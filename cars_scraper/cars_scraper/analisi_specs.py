# -*- coding: utf-8 -*-

# due args: marca e nome file

from pony.orm import *
from orm import *
import sys
import re

import pandas as pd


def get_val(model_spec):
    if model_spec.tipo == "caratteristica":
        return model_spec.value
    elif model_spec.tipo == "di serie":
        return "x"
    elif model_spec.tipo == "optional":
        return "O"

@db_session
def genera_sheet_specs():
    models = select(m for m in Model if m.marca == sys.argv[2])

    final_df = pd.DataFrame()

    for model in models:
        df = pd.DataFrame(
            [('model', model.name), ('anno', model.years), ('prezzo', model.price), ('link scheda', model.url)]
            + [(x.spec.label, get_val(x)) for x in model.specs_t])
        df = df.set_index(0)  # setto le label come index
        df = df.rename(columns={1: model.name})

        final_df = pd.concat([final_df, df], axis=1)

    return final_df
        # rename prima colonna con nome model



def find_specs_che_hanno_tutti():
    final_df = genera_sheet_specs()
    df_specs_comuni = final_df[~final_df.isnull().any(axis=1)]
    import pdb; pdb.set_trace()

@db_session
def trova_differenze_in_modelli_uguali():
    versions = select(m.name for m in Model if sys.argv[3] in m.name)
    specs = select(s for s in Spec)

    for v in versions:
        differenti = []

        vmodels = select(m for m in Model if m.name == v).order_by(Model.start_date)
        print [x.name for x in vmodels]
        print [x.years for x in vmodels]
        for spec in specs:
            vals = [get_spec_val(spec, m) for m in vmodels]
            if not check_equal(vals):
                print spec.label.encode(sys.stdout.encoding, errors='replace'), vals


def get_spec_val(spec, model):
    model_specs = select(ms for ms in ModelSpec if ms.model == model and ms.spec == spec)
    if model_specs.count():
        return get_val(model_specs.first())
    return None

def check_equal(iterator):
   return len(set(iterator)) <= 1


@db_session
def fix_previous_model_years():

    models = select(m for m in Model if m.start_date is None)

    regex = r"(\d{2}\/\d{4})"

    for model in models:
        matches = re.findall(regex, model.years)
        if len(matches) == 2:
            model.start_date = datetime.strptime(matches[0], "%m/%Y").date()
            model.end_date = datetime.strptime(matches[1], "%m/%Y").date()


cmds = {
    'find_specs_che_hanno_tutti': find_specs_che_hanno_tutti,
    'trova_differenze_in_modelli_uguali': trova_differenze_in_modelli_uguali,
    'fix_previous_model_years': fix_previous_model_years
}

cmds[sys.argv[1]]()